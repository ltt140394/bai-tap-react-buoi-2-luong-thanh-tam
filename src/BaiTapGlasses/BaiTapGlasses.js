import React, { Component } from 'react'
import GlassesData from "./dataGlasses.json"
import "./baiTapGlasses.css"

export default class BaiTapGlasses extends Component {
    state = {
        glassesImgUrl: "",
        glassesName: "",
        glassesPrice: "",
        glassesDesc: "",
    }

    handleChangeClasses = (id) => {
        let index = GlassesData.findIndex((item) => {
            return item.id == id;
        });

        this.setState({
            glassesImgUrl: GlassesData[index].url,
            glassesName: GlassesData[index].name,
            glassesPrice: "$" + GlassesData[index].price,
            glassesDesc: GlassesData[index].desc,
        });
        document.getElementById("glassesInfo").style.display = "block"
    }

    render() {
        return (
            <div className="glasses-container">
                <h2 className="title">TRY GLASSES APP ONLINE</h2>
                <div className="container">
                    <div className="model">
                        <div className="glassesPicked">
                            <img src={this.state.glassesImgUrl} alt="" />
                        </div>
                        <div className="glassesInfo" id="glassesInfo">
                            <span className="glassesName">{this.state.glassesName}</span>
                            <span className="glassesPrice">{this.state.glassesPrice}</span>
                            <p className="glassesDesc">{this.state.glassesDesc}</p>
                        </div>
                    </div>
                    <div className="glassesList row" id="glassesList">
                        {GlassesData.map((item) => {
                            return <div className="col-4" key={item.id}>
                                <img className="w-75 mb-3" src={item.url} alt="" onClick={() => {
                                    this.handleChangeClasses(item.id);
                                }} />
                            </div>
                        })}
                    </div>
                </div>
            </div>
        )
    }
}
